package ru.yaleksandrova.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.yaleksandrova.tm.command.AbstractCommand;
import ru.yaleksandrova.tm.util.ApplicationUtil;

public class LoginCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "login";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Login user";
    }

    @Override
    public void execute() {
        System.out.println("[LOGIN]");
        System.out.println("ENTER LOGIN:");
        @NotNull final String login = ApplicationUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        @NotNull final String password = ApplicationUtil.nextLine();
        serviceLocator.getAuthService().login(login, password);
    }

}
