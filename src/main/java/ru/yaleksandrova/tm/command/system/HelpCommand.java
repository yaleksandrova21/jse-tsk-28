package ru.yaleksandrova.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.yaleksandrova.tm.command.AbstractCommand;

public final class HelpCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "help";
    }

    @NotNull
    @Override
    public String arg() {
        return "-h";
    }

    @NotNull
    @Override
    public String description() {
        return "Display list of commands";
    }

    @Override
    public void execute() {
        System.out.println("[HELP]");
        for (final AbstractCommand command : serviceLocator.getCommandService().getCommands()) {
            System.out.println(command);
        }
    }

}
