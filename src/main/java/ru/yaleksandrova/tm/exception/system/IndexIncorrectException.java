package ru.yaleksandrova.tm.exception.system;

import ru.yaleksandrova.tm.exception.AbstractException;

public class IndexIncorrectException extends AbstractException {

    public IndexIncorrectException() {
        super("Error! This value is incorrect");
    }

    public IndexIncorrectException(final Throwable cause) {
        super(cause);
    }

    public IndexIncorrectException(final String value) {
        super("Error! This value `" + value + "` is not number");
    }

}
