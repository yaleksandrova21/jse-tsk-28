package ru.yaleksandrova.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.yaleksandrova.tm.api.repository.IProjectRepository;
import ru.yaleksandrova.tm.enumerated.Status;
import ru.yaleksandrova.tm.exception.entity.ProjectNotFoundException;
import ru.yaleksandrova.tm.model.Project;

import java.util.Date;

public final class ProjectRepository extends AbstractOwnerRepository<Project> implements IProjectRepository {

    @NotNull
    @Override
    public Project findByName(@NotNull String userId, @NotNull String name) {
        return list.stream()
                .filter(o -> userId.equals(o.getUserId()) && name.equals(o.getName()))
                .findFirst().orElseThrow(ProjectNotFoundException::new);
    }

    @NotNull
    @Override
    public Project removeByName(@NotNull String userId, @NotNull String name) {
        final Project project = findByName(userId, name);
        if (project == null)
            return null;
        list.remove(project);
        return project;
    }

    @Nullable
    @Override
    public Project startByIndex(@NotNull String userId, @NotNull Integer index) {
        final Project project = findByIndex(userId, index);
        if (project == null)
            return null;
        project.setStatus(Status.IN_PROGRESS);
        project.setStartDate(new Date());
        return project;
    }

    @Nullable
    @Override
    public Project startByName(@NotNull String userId, @NotNull String name) {
        final Project project = findByName(userId, name);
        if (project == null)
            return null;
        project.setStatus(Status.IN_PROGRESS);
        project.setStartDate(new Date());
        return project;
    }

    @Nullable
    @Override
    public Project startById(@NotNull String userId, @NotNull String id) {
        final Project project = findById(userId, id);
        if (project == null)
            return null;
        project.setStatus(Status.IN_PROGRESS);
        project.setStartDate(new Date());
        return project;
    }

    @Nullable
    @Override
    public Project finishById(@NotNull String userId, @NotNull String id) {
        final Project project = findById(userId, id);
        if (project == null)
            return null;
        project.setStatus(Status.COMPLETED);
        project.setFinishDate(new Date());
        return project;
    }

    @Nullable
    @Override
    public Project finishByIndex(@NotNull String userId, @NotNull Integer index) {
        final Project project = findByIndex(userId, index);
        if (project == null)
            return null;
        project.setStatus(Status.COMPLETED);
        project.setFinishDate(new Date());
        return project;
    }

    @Nullable
    @Override
    public Project finishByName(@NotNull String userId, @NotNull String name) {
        final Project project = findByName(userId, name);
        if (project == null)
            return null;
        project.setStatus(Status.COMPLETED);
        project.setFinishDate(new Date());
        return project;
    }

    @Nullable
    @Override
    public Project changeStatusById(@NotNull String userId, @NotNull String id, @NotNull Status status) {
        final Project project = findById(userId, id);
        if (project == null)
            return null;
        project.setStatus(status);
        return project;
    }

    @Nullable
    @Override
    public Project changeStatusByIndex(@NotNull String userId, @NotNull Integer index, @NotNull Status status) {
        final Project project = findByIndex(userId, index);
        if (project == null)
            return null;
        project.setStatus(status);
        return project;
    }

    @Nullable
    @Override
    public Project changeStatusByName(@NotNull String userId, @NotNull String name, @NotNull Status status) {
        final Project project = findByName(userId, name);
        if (project == null)
            return null;
        project.setStatus(status);
        return project;
    }

}
