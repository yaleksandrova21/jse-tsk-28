package ru.yaleksandrova.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.yaleksandrova.tm.api.repository.IUserRepository;
import ru.yaleksandrova.tm.model.User;

import java.util.ArrayList;
import java.util.List;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Nullable
    @Override
    public User findByLogin(@Nullable final String login) {
        return list.stream()
                .filter(o -> login.equals(o.getLogin()))
                .findFirst().orElse(null);
    }

    @Nullable
    @Override
    public User findByEmail(@NotNull String email) {
        return list.stream()
                .filter(o -> email.equals(o.getEmail()))
                .findFirst().orElse(null);
    }

    @NotNull
    @Override
    public User removeUserById(@NotNull final String id) {
        final User user = findById(id);
        if (user == null) return null;
        list.remove(user);
        return user;
    }

    @Nullable
    @Override
    public User removeUserByLogin(@NotNull final String login) {
        final User user = findByLogin(login);
        if (user == null) return null;
        list.remove(user);
        return user;
    }

}
